package com.geekplus;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author hufeng
 * @date 2018/9/10 21:15
 * @describe
 */
public class OverallLock implements Serializable {

    private static final long serialVersionUID = 6377006834401683842L;

    /**
     * 锁名
     */
    private String lockName;

    /**
     * 开始时间
     */
    private Long startTime;

    /**
     * 线程id
     */
    private Long threadId;

    public String getLockName() {
        return lockName;
    }

    public void setLockName(String lockName) {
        this.lockName = lockName;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getThreadId() {
        return threadId;
    }

    public void setThreadId(Long threadId) {
        this.threadId = threadId;
    }

    public OverallLock(String lockName, Long startTime, Long threadId) {
        this.lockName = lockName;
        this.startTime = startTime;
        this.threadId = threadId;
    }

    @Override
    public String toString() {
        return "OverallLock{" +
                "lockName='" + lockName + '\'' +
                ", startTime=" + startTime +
                ", threadId=" + threadId +
                '}';
    }

    ;
}
