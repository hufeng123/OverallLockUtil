package com.geekplus;

import net.sf.ehcache.constructs.blocking.LockTimeoutException;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author hufeng
 * @date 2018/9/10 21:12
 * @describe 全局锁工具 可重入性、重试机制、非持久性
 */
public class OverallLockUtil {

    private static Map<String, OverallLock> map    = new ConcurrentHashMap();

    /**
     * 重试次数
     */
    private static final AtomicInteger TRY_COUNT = new AtomicInteger(3);

    /**
     * 生命周期
     */
    private static final Long EXPIRATION_TIME   = 200000L;

    /**
     * 重试间隔
     */
    private static final Long INTERVAL_TIME     = 2000L;

    /**
     *  unlock
     * @param key
     * @return
     */
    public static OverallLock unLock(String key) {
            return map.remove(key);
    }

    /**
     *  get key by param. if table unlock, return new lock, else throw LockTimeoutException
     * @param key
     * @param startTime
     * @param threadId
     * @return
     */
    public static OverallLock getLock(String key, Long startTime, Long threadId) throws InterruptedException, LockTimeoutException {

        synchronized (key) {
            System.out.println(threadId + " try--------------------------------------------" + key.hashCode());
            System.out.println(map);

            // reentrant
            if (map.get(key) != null && Thread.currentThread().getId() == map.get(key).getThreadId()) {
                return map.get(key);
            }
            return tryAgain(key, startTime, threadId, new AtomicInteger(TRY_COUNT.getAndIncrement()));
        }

    }

    private static OverallLock tryAgain(String key, Long startTime, Long threadId, AtomicInteger count) throws InterruptedException, LockTimeoutException {

        if (map.get(key) != null) {
            // lazy check
            if (System.currentTimeMillis() >= map.get(key).getStartTime() + EXPIRATION_TIME) {
                unLock(key);
                lock(key, startTime, threadId);
                return map.get(key);
            }

            while (count.get() > 0) {
                Thread.sleep(INTERVAL_TIME);
                count.decrementAndGet();
                System.out.println(threadId+"tryCount:"+count.get());
                return tryAgain(key, startTime, threadId, count);
            }
            throw new LockTimeoutException();

        }
        lock(key, startTime, threadId);
        System.out.println(threadId+"---------------------suo");
        return map.get(key);
    }

    /**
     *  lock by param
     * @param lockName
     * @param startTime
     * @param threadId
     * @return
     */
    private static void lock(String lockName, Long startTime, Long threadId) {
        map.put(lockName, new OverallLock(lockName, startTime, threadId));
    }

}
