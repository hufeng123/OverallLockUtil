package com.geekplus;
import net.sf.ehcache.constructs.blocking.LockTimeoutException;

/**
 * @author hufeng
 * @date 2018/9/10 18:56
 * @describe case：两个线程 同时操作一张表
 */
public class testbbb {
    public static void main(String[] args){

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("t2 running");
                OverallLock lock = null;
                try {
                    // 获取锁
                    lock = OverallLockUtil.getLock("base_table", System.currentTimeMillis(), 222L);
                    System.out.println("t2 getLock:" + lock);
                    // 锁未占用
                    if (lock != null) {
                        // invoke
                        System.out.println("base_table insert ----------------doing");
                        Thread.currentThread().sleep(10000L);
                        System.out.println("base_table insert ----------------end");
                        // 释放锁
                        OverallLockUtil.unLock("base_table");
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }catch (LockTimeoutException e){
                    // 回滚
                    System.out.println("t2回滚上下文");
                }
            }
        });


        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("t3 running");
                OverallLock lock = null;
                try {

                    // 获取锁
                    lock = OverallLockUtil.getLock("base_table", System.currentTimeMillis(), 333L);
                    System.out.println("t3 getLock:" + lock);
                    // 锁未占用
                    if (lock != null) {
                        // invoke
                        System.out.println("base_table update ----------------doing");
                        Thread.currentThread().sleep(10000L);
                        System.out.println("base_table update ----------------end");
                        // 释放锁
                        OverallLockUtil.unLock("base_table");
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }catch (LockTimeoutException e){
                    // 回滚
                    System.out.println("t3回滚上下文");
                }
            }
        });

        t2.start();
        t3.start();

//        t2 running
//        t3 running
//        333 try---------------------------------------------474708768
//        333---------------------suo
//        t3 getLock:OverallLock{lockName='base_table', startTime=1536662041335, threadId=333}
//        base_table update ----------------doing
//        222 try---------------------------------------------474708768
//        t2回滚上下文
//        base_table update ----------------end

    }
}
