package com.geekplus;
import net.sf.ehcache.constructs.blocking.LockTimeoutException;

/**
 * @author hufeng
 * @date 2018/9/10 18:56
 * @describe case：一个线程上下文中操作A表，另一个线程同时操作A表
 */
public class testddd {
    public static void main(String[] args){

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("t2 running");
                OverallLock lock = null;
                try {
                    // 获取锁
                    lock = OverallLockUtil.getLock("base_table", System.currentTimeMillis(), Thread.currentThread().getId());
                    System.out.println("t2-1 getLock:" + lock);
                    // 锁未占用
                    if (lock != null) {
                        // invoke
                        System.out.println("base_table insert ----------------doing");
                        Thread.currentThread().sleep(5000L);
                        System.out.println("base_table insert ----------------end");
                        // 释放锁
//                        System.out.println("222L unlock");
//                        OverallLockUtil.unLock("base_table");
                    }

                    // 获取锁
                    lock = OverallLockUtil.getLock("base_table", System.currentTimeMillis(), Thread.currentThread().getId());
                    System.out.println("t2 getLock --:" + lock);
                    // 锁未占用
                    if (lock != null) {
                        // invoke
                        System.out.println("base_table insert ----------------doing");
                        Thread.currentThread().sleep(5000L);
                        System.out.println("base_table insert ----------------end");
                        // 释放锁
                        System.out.println("222L unlock");
                        OverallLockUtil.unLock("base_table");
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }catch (LockTimeoutException e){
                    // 回滚
                    System.out.println("t2回滚上下文");
                }
            }
        });


        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("t3 running");
                OverallLock lock = null;
                try {

                    // 获取锁
                    lock = OverallLockUtil.getLock("base_table", System.currentTimeMillis(), Thread.currentThread().getId());
                    System.out.println("t3 getLock:" + lock);
                    // 锁未占用
                    if (lock != null) {
                        // invoke
                        System.out.println("base_table update ----------------doing");
                        Thread.currentThread().sleep(5000L);
                        System.out.println("base_table update ----------------end");
                        // 释放锁
                        System.out.println("333L unlock");
                        OverallLockUtil.unLock("base_table");
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }catch (LockTimeoutException e){
                    // 回滚
                    System.out.println("t3回滚上下文");
                }
            }
        });

        t3.start();
        t2.start();

//        t3 running
//        t2 running
//        12 try---------------------------------------------474708768
//        {}
//        12---------------------suo
//        13 try---------------------------------------------474708768
//        t2-1 getLock:OverallLock{lockName='base_table', startTime=1536665991004, threadId=12}
//        base_table insert ----------------doing
//        {base_table=OverallLock{lockName='base_table', startTime=1536665991004, threadId=12}}
//        13tryCount:3
//        base_table insert ----------------end
//        13tryCount:2
//        13tryCount:1
//        13tryCount:0
//        t3回滚上下文
//        12 try---------------------------------------------474708768
//        {base_table=OverallLock{lockName='base_table', startTime=1536665991004, threadId=12}}
//        t2 getLock --:OverallLock{lockName='base_table', startTime=1536665991004, threadId=12}
//        base_table insert ----------------doing
//        base_table insert ----------------end
//        222L unlock

    }
}
